package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Action action) {
        action.execute(this.location);
    }

    public Location getDirection() {
        return this.location;
    }

    public void executeBatCommand(List<Action> commandList) {
        commandList.forEach(this::executeCommand);
    }
}
