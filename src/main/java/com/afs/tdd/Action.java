package com.afs.tdd;

public interface Action {
    public void execute(Location location);
}
