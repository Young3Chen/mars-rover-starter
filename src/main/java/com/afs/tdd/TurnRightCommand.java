package com.afs.tdd;

public class TurnRightCommand implements Action {

    @Override
    public void execute(Location location) {
        location.setDirectionIndex((location.getDirectionIndex() + 5) % 4);
    }
}
