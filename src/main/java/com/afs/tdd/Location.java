package com.afs.tdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Location {
    private int coordinateX;
    private int coordinateY;

    private int directionIndex;
    private final List<Direction> directions;

    public Location(int coordinateX, int coordinateY, Direction direction) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.directions = new ArrayList<>();
        directions.addAll(Arrays.asList(Direction.North, Direction.East, Direction.South, Direction.West));
        this.directionIndex = this.directions.indexOf(direction);
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public Direction getDirection() {
        return this.directions.get(directionIndex);
    }

    public void setCoordinateY(int newCoordinateY) {
        this.coordinateY = newCoordinateY;
    }

    public void setCoordinateX(int newCoordinateX) {
        this.coordinateX = newCoordinateX;
    }

    public void setDirectionIndex(int directionIndex) {
        this.directionIndex = directionIndex;
    }

    public int getDirectionIndex() {
        return this.directionIndex;
    }
}
