package com.afs.tdd;

public class TurnLeftCommand implements Action{
    @Override
    public void execute(Location location) {
        location.setDirectionIndex((location.getDirectionIndex() + 3) % 4);
    }
}
