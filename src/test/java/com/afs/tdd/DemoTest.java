package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class DemoTest {
    @Test
    void should_change_y_1_when_execute_executeCommand_given_command_M_and_Direction_North() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new MoveCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(1, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.North, marsRover.getDirection().getDirection());
    }

    @Test
    void should_change_y_when_execute_executeCommand_given_command_M_and_Direction_South() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new MoveCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(-1, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.South, marsRover.getDirection().getDirection());
    }

    @Test
    void should_change_x_1_when_execute_executeCommand_given_command_M_and_Direction_West() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new MoveCommand());

        Assertions.assertEquals(-1, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.West, marsRover.getDirection().getDirection());
    }

    @Test
    void should_change_x_1_when_execute_executeCommand_given_command_M_and_Direction_East() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new MoveCommand());

        Assertions.assertEquals(1, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.East, marsRover.getDirection().getDirection());
    }

    @Test
    void should_turn_left_when_execute_executeCommand_given_command_L_and_Direction_North() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new TurnLeftCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.West, marsRover.getDirection().getDirection());
    }

    @Test
    void should_turn_left_when_execute_executeCommand_given_command_L_and_Direction_South() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new TurnLeftCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.East, marsRover.getDirection().getDirection());
    }

    @Test
    void should_turn_left_when_execute_executeCommand_given_command_L_and_Direction_East() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new TurnLeftCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.North, marsRover.getDirection().getDirection());
    }

    @Test
    void should_turn_left_when_execute_executeCommand_given_command_L_and_Direction_West() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new TurnLeftCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.South, marsRover.getDirection().getDirection());
    }

    @Test
    void should_turn_right_when_execute_executeCommand_given_command_L_and_Direction_West() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new TurnRightCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.North, marsRover.getDirection().getDirection());
    }

    @Test
    void should_turn_right_when_execute_executeCommand_given_command_L_and_Direction_East() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new TurnRightCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.South, marsRover.getDirection().getDirection());
    }

    @Test
    void should_turn_right_when_execute_executeCommand_given_command_L_and_Direction_North() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new TurnRightCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.East, marsRover.getDirection().getDirection());
    }

    @Test
    void should_turn_right_when_execute_executeCommand_given_command_L_and_Direction_South() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        marsRover.executeCommand(new TurnRightCommand());

        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(0, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.West, marsRover.getDirection().getDirection());
    }

    @Test
    void should_change_direction_when_execute_executeBatCommand_given_commanList_R_M_L_M_L_and_North() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        List<Action> commandList = new ArrayList<>();
        commandList.add(new TurnRightCommand());
        commandList.add(new MoveCommand());
        commandList.add(new TurnLeftCommand());
        commandList.add(new MoveCommand());
        commandList.add(new TurnLeftCommand());

        marsRover.executeBatCommand(commandList);
        Assertions.assertEquals(1, marsRover.getDirection().getCoordinateX());
        Assertions.assertEquals(1, marsRover.getDirection().getCoordinateY());
        Assertions.assertEquals(Direction.West, marsRover.getDirection().getDirection());
    }

}
